var baseObj = {};
var baseFunc = {};

/****数据验证****/
baseFunc.valide  = function(obj) {
	var resObj = {};
	if(obj.name != undefined) { //宝宝姓名
		var namePattern = /^[\u4E00-\u9FA5]{1,5}$/;
		if(!namePattern.test(obj.name)) {
			resObj.name = false;
		} else {
			resObj.name = true;
		}
	}

	if(obj.birthday != undefined) { //出生年月
		var borthPattern =/^(\d{4})-(\d{2})-(\d{2})$/;
		if(!borthPattern.test(obj.birthday)) {
			resObj.birthday = false;
		} else {
			resObj.birthday = true;
		}
	}

	if(obj.tel != undefined) { //电话
		var telPartten = /^1[3,5,7,8,9]\d{9}$/;
		if(!telPartten.test(obj.tel)) {
			resObj.tel = false;
		} else {
			resObj.tel = true;
		}
	}

	if(obj.classNum.formal != undefined) { //课时包
		if(obj.classNum.formal !== "") {
			resObj.classNum = true;
		} else {
			resObj.classNum = false;
		}
	}

	if(obj.price != undefined) { //价格
		if(obj.price !== "") {
			resObj.price = true;
		} else {
			resObj.price = false;
		}
	}

	if(obj.buyDay != undefined) { //购买日期
		if(obj.buyDay !== "") {
			resObj.buyDay = true;
		} else {
			resObj.buyDay = false;
		}
	}
	
	if(obj.period != undefined) { //有效期
		if(obj.period !== "") {
			resObj.period = true;
		} else {
			resObj.period = false;
		}
	}

	return resObj;
};

/*********** 页面跳转 ********/
baseFunc.locationSkip = function(link) {
	var href = window.location.href,
		pos = href.lastIndexOf("/") + 1,
		str = href.slice(0, pos);

	window.location.href = str + link;
};

/*********** 删除本地数据 ********/
baseFunc.deleteStorageData = function(name, obj) {
	if (!name || !obj) {
		return;
	};
	var data = baseFunc.getStorageData(name);
	if (name == "schedule") {
		// localStorage.removeItem(name);
		var idx = 0;
		for (var i = 0; i < data.length; i++) {
			if(data[i].date == obj.date) {
				idx = i;
				break;
			}
		};
		data.splice(i, 1);
		localStorage[name] = JSON.stringify(data);
	};
};

/********* 储存本地数据 **********/
var iidd = 1;
baseFunc.setStorageData = function(name, obj) {
	var o = {},
		LS = localStorage,
		arr = [],
		memberFlag = "";
	
	iidd++;
	if(LS[name]) {
		var oldData = JSON.parse(LS[name]);
		if(name == "member") {
			memberFlag = obj.name + ((new Date()).valueOf() + iidd);
			obj["flag"] = memberFlag;
		}
		oldData.push(obj);
		localStorage[name] = JSON.stringify(oldData);
	} else {
		if(name == "member") {
			memberFlag = obj.name + (new Date()).valueOf();
			obj["flag"] = memberFlag;
		}
		if(obj.length != 0 && !(obj instanceof Array)) {
			arr.push(obj);
		}
		localStorage[name] = JSON.stringify(arr);
	}
};

/*********修改本地数据**********/
baseFunc.editStorageData = function(name, obj) {
	var olData = localStorage[name];
	olData = JSON.parse(olData);

	if(name == "member") {
		var flag = obj.flag,
			newData = {},
			findPosition = 0;
		for(var i = 0; i < olData.length; i++) {
			if(olData[i]["flag"] == flag) {
				findPosition = i;
				newData = $.extend(true, olData[i], obj);
				break;
			}
		}
		olData.splice(findPosition, 1, newData);
		localStorage[name] = JSON.stringify(olData);
	} else {
		var infoData;
		for (var k = 0; k < olData.length; k++) {
			if(olData[k].date == obj.date) {
				olData.splice(k, 1, obj);
				break;
			}
		};
		localStorage[name] = JSON.stringify(olData);
	}
};

/*********获取本地数据**********/
baseFunc.getStorageData = function(keyName, obj, searchType) { //{name||birthday||faName||maName||tel||classNum, "search"}
	var newObj = {},
		resArr = [],
		tempObj = {},
		LS;

	if(localStorage[keyName]) {
		LS = JSON.parse(localStorage[keyName])
	} else {
		return null;
	}

	if(keyName && !obj && !searchType) {
		return LS;
	}

	if(!obj || !Object.keys(obj)) { //没有传参或者传参为空，返回全部
		for(var i = 0; i < LS.length; i++) {
			newObj[LS[i].flag] = LS[i];
		}
		return newObj;
	}

	//if(!handle && obj.flag) {//返回数组
	if(obj && !searchType) {
		for(var i = 0; i < LS.length; i++) {
			tempObj = LS[i];
			$.each(obj, function(key, value) {
				if(tempObj[key] == value) {
					resArr.push(LS[i]);
				}
			})
		}
		return resArr;
	}

	if(searchType == "dim") { //模糊搜索
		for(var i = 0; i < LS.length; i++) {
			tempObj = LS[i];
			$.each(obj, function(key, value) {
				if(tempObj[key].indexOf(value) != -1) {
					resArr.push(LS[i]);
				}
			})
		}
		return resArr;
	}

	if(handle == "search"){ //搜索
		Object.getOwnPropertyNames(LS).forEach(function(key) {
			var parseData = JSON.parse(LS[key]),
				obji = 0;
			Object.getOwnPropertyNames(parseData).forEach(function(k) {
				var objLength = Object.keys(obj).length;
				Object.getOwnPropertyNames(obj).forEach(function(okey) {
					if(!Array.isArray(obj[okey])) {
						if(okey == k && parseData[k] == obj[okey]) {
							obji++;
							if(obji == objLength) {
								resArr.push(parseData);
							}
						}
					}
				});
			});
		});
		return resArr;
	}
};

/*********删除本地数据**********/
baseFunc.deleteMemberStorageData = function(obj) {
	var LS = JSON.parse(localStorage["member"]),
		key = Object.keys(obj)[0],
		ids = -1;

	for (var i = 0; i < LS.length; i++) {
		if(LS[i][key] == obj[key]) {
			ids = i;
			break;
		}
	}
	if(ids == -1) {
		return;
	}
	LS.splice(ids, 1);
	localStorage["member"] = JSON.stringify(LS);
};

/*********储存session数据**********/
baseFunc.setSessionData = function(key, name) {
	if(!key || !name) {
		alert("缺少参数");
		return;
	}
	sessionStorage.setItem(key, name);
	return true;
};

/*********获取session数据**********/
baseFunc.getSessionData = function(key) {
	if(!key) {
		alert("缺少参数");
		return;
	}
	return sessionStorage.getItem(key);
};

/*************计算两个日期相差天数***********/
baseFunc.dateDiff = function(date1, date2){
    var dt1 = new Date(Date.parse(date1.replace(/-/g, '/')));
    var dt2 = new Date(Date.parse(date2.replace(/-/g, '/')));
    try {
        return Math.round((dt2.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24));
    }
    catch (e) {
        return e.message;
    }
};

/************* 弹出提示 ***********/
var alertStatus = false;
baseFunc.alert = function(txt, type) {
	var htm;
	if(type == "green") {
		htm = "<div class='cd-popup'><div class='cd-popup-container cd-green'>"+txt+"</div></div>";
	} else {
		htm = "<div class='cd-popup'><div class='cd-popup-container'>"+txt+"</div></div>";	
	}
	
	if(alertStatus) {
		return;
	}
	alertStatus = true;

	$(".cd-popup").remove();
	$("body").append(htm);
	// if($(".cd-popup").length > 0) {
	// 	$("body").append(htm);
	// } else {
	// 	$(".cd-popup-container").text(txt);
	// }
	
	setTimeout(function() {
		$(".cd-popup").addClass("is-visible");
	}, 100);

	setTimeout(function() {
		$(".cd-popup").removeClass("is-visible");
		alertStatus = false;
	}, 1000);
};

/*********左侧菜单********/
(function() {
	var vm = avalon.define({
	    $id: "navBar"
	})

	vm.navTitleArr = {
		1: {
			par: "会员管理",
			child: ["添加会员", "会员表", "FreeClass管理", "试听管理"],
			href: ["addMember.html", "allMember.html", "freeClass.html", "audition.html"]
		},
		2: {
			par: "销课管理",
			child: ["查询课表", "添加课表", "添加上课人员", "会员销课", "活动销课"],
			href: ["searchSchedule.html", "addSchedule.html", "addClassMember.html", "passClass.html", "activityAllMember.html"]
		},
		3: {
			par: "内部管理",
			child: ["工资计算"],
			href: ["salary.html"]
		}
	};
})();

/****输入电话提示框****/
(function() {
	$("input[type=tel]").focus(function() {
		var offset = $(this).offset(),
			width = $(this).width() - 8,
			height = $(this).outerHeight(),
			top = offset.top - height -7,
			word = $(this).val(),
			element = "<div class='tips-dialog' style='border:1px solid #ccc; color: #c0392b; font-size: 24px; padding: 0 10px; font-weight: bold; position: absolute; height: 40px; line-height:40px; background: #ecf0f1; width: "+width+"px; left: "+offset.left+"px; top: "+top+"px'>"+word+"</div>";

		$(this).after(element);
	});

	$("input[type=tel]").keyup(function() {
		var word = $(this).val();
		$(".tips-dialog").text(word);
	});

	$("input[type=tel]").focusout(function() {
		$(".tips-dialog").remove();
	});
})();




