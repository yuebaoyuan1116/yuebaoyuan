function weeklyCalendar(dataArr, options, showData) {
    var that = this, 
        _date = new Date(), 
        currentDay = _date.getDay(), 
        weekNum = $("#week_title").attr("ids");
    var description = {
        firstTitle: '观看直播',
        nextTitle: '下载讲义',
        listTitle: '讲义列表',
        methodsTitle: '下载'
    }
    var format = function(num) {
        var _f = num < 10 ? '0' + num : num;
        return _f
    }
    var createWeek = function() {
        var lis = '';
        var weeks_ch = ['一', '二', '三', '四', '五', '六', '日'];
        for (var i = 0, len = weeks_ch.length; i < len; i++) {
            lis += '<li>' + weeks_ch[i] + '</li>';
        }
        ;$("#weekUl").html(lis);
    }
    var countTime = function(n) {
        var getTime = _date.getTime() + (24 * 60 * 60 * 1000) * n;
        var needDate = new Date(getTime);
        var getYear = needDate.getFullYear();
        var getMonth = needDate.getMonth() + 1;
        var getDate = needDate.getDate();
        var obj = {
            'year': getYear,
            'month': getMonth,
            'date': getDate
        };
        return obj
    }
    var createDate = function(cDay) {
        var _cDay = cDay + 1;
        var dateHtml = '';
        var currY = format(_date.getFullYear())
          , currM = format(_date.getMonth() + 1)
          , currD = format(_date.getDate());
        for (var i = _cDay; i < _cDay + 7; i++) {
            if (currY == countTime(i).year && currM == countTime(i).month && currD == countTime(i).date) {
                dateHtml += '<section data-year=' + countTime(i).year + ' data-month=' + countTime(i).month + ' data-date=' + countTime(i).date + '>' + '<h2 class="current">' + '<a href="javascript:void(0)">' + format(countTime(i).date) + '</a>' + '</h2>' + '<ul class="optionsUl" data-year=' + countTime(i).year + ' data-month=' + countTime(i).month + ' data-date=' + countTime(i).date + '>' + '</ul>' + '</section>'
            } else {
                dateHtml += '<section data-year=' + countTime(i).year + ' data-month=' + countTime(i).month + ' data-date=' + countTime(i).date + '>' + '<h2>' + '<a href="javascript:void(0)">' + format(countTime(i).date) + '</a>' + '</h2>' + '<ul class="optionsUl" data-year=' + countTime(i).year + ' data-month=' + countTime(i).month + ' data-date=' + countTime(i).date + '>' + '</ul>' + '</section>'
            }
        }
        $("#calendarBox").html(dateHtml);
        reminder(); 
    }
    var reminder = function() {
        var optionsUl = $(".optionsUl");
        $.each(optionsUl, function(index, element) {
            var optionsHtml = '';
            let $element = $(element);
            let ele_year = format($element.attr('data-year'))
              , ele_month = format($element.attr('data-month'))
              , ele_date = format($element.attr('data-date'));
            $.each(dataArr, function(ind, ele) {
                let show_date = ele.date.split('-');
                if (parseInt(ele_year) == parseInt(show_date[0]) && parseInt(ele_month) == parseInt(show_date[1]) && parseInt(ele_date) == parseInt(show_date[2])) {
                    $element.prev().addClass("active");
                    $.each(ele.items, function(_index, _ele) {
                        optionsHtml += '<li>' + '<p>' + _ele.className + '</p>'
                        if (showData) {
                            $.each(_ele.downList, function(index_, element_) {
                                optionsHtml += '<p>' + element_.downName + '<a data-address="' + element_.downAddress + '" href="javascript:void(0)">' + description.methodsTitle + '</a></p>'
                            })
                            optionsHtml += '</div>' + '<h5 onclick="closeDownList(this)"></h5>'
                        }
                        optionsHtml += '</li>'
                    })
                    return true
                }
            })
            $(element).html(optionsHtml);
        })
    }
    var changeWeek = function(weekNum) {
        createDate(-currentDay + (7 * weekNum));
        $("#week_title").attr("ids", weekNum);
        titleTime();
    }
    $("#prevWeek").on("click", function() {
        weekNum--;
        changeWeek(weekNum);
    })
    $("#nextWeek").on("click", function() {
        weekNum++;
        changeWeek(weekNum);
    })
    var titleTime = function() {
        var section = $("#calendarBox").find("section");
        var titleHtml = '';
        titleHtml += format($(section[0]).attr('data-year')) + '年' + format($(section[0]).attr('data-month')) + '月' + format($(section[0]).attr('data-date')) + '日 - ' + format($(section[6]).attr('data-year')) + '年' + format($(section[6]).attr('data-month')) + '月' + format($(section[6]).attr('data-date')) + '日';
        $("#showDate").html(titleHtml);
    }
    $("#calendarBox").on("click", "h2", function() {
        var textDate = $(this).parent().attr("data-year") + "-" + $(this).parent().attr("data-month") + "-" + $(this).parent().attr("data-date");
        $(this).addClass("select");
        $(this).parent().siblings().find('h2').removeClass("select");
        options['clickDate'](textDate);
    })
    var initWeeklyCalendar = function() {
        createWeek();
        createDate(-currentDay);
        titleTime();
    }()
    $(".jy_list").on("click", "a", function() {
        options['clickDownLoad'](this);
    })
}
function downListShow(that) {
    that.parentNode.parentNode.getElementsByClassName('down_list')[0].style.display = "block";
}
function downListHide(that) {
    that.style.display = "none";
}
function closeDownList(that) {
    that.parentNode.parentNode.style.display = "none";
}
function stopmp(e) {
    window.event ? window.event.cancelBubble = true : e.stopPropagation();
}